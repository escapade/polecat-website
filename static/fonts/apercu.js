@font-face {
  font-family: 'apercu-black';
  src: url(../fonts/apercu_black/apercu-black.ttf) format('truetype');
  font-weight: normal;
  font-style: normal;
}
  `

  @font-face {
    font-family: 'apercu';
    src: url(../fonts/apercu_black/apercu-black.eot);
    src: url(../fonts/apercu_black/apercu-black.woff) format('woff'),
        url(../fonts/apercu_black/apercu-black.woff2) format('woff2'),
        url(../fonts/apercu_black/apercu-black.ttf) format('truetype');
    font-weight: 800;
    font-style: normal;
  }
  
  @font-face {
    font-family: 'apercu';
    src: url(../fonts/apercu_bold/apercu-bold.eot);
    src: url(../fonts/apercu_bold/apercu-bold.eot?#iefix) format('embedded-opentype'),
        url(../fonts/apercu_bold/apercu-bold.woff) format('woff'),
        url(../fonts/apercu_bold/apercu-bold.woff2) format('woff2'),
        url(../fonts/apercu_bold/apercu-bold.ttf) format('truetype');
    font-weight: 600;
    font-style: normal;
  }
  
  @font-face {
    font-family: 'apercu';
    src: url(../fonts/apercu_light/apercu-light.eot);
    src: url(../fonts/apercu_light/apercu-light.eot?#iefix) format('embedded-opentype'),
        url(../fonts/apercu_light/apercu-light.woff) format('woff'),
        url(../fonts/apercu_light/apercu-light.woff2) format('woff2'),
        url(../fonts/apercu_light/apercu-light.ttf) format('truetype');
    font-weight: 100;
    font-style: normal;
  }
  
  @font-face {
    font-family: 'apercu';
    src: url(../fonts/apercu_medium/apercu-medium.eot);
    src: url(../fonts/apercu_medium/apercu-medium.eot?#iefix) format('embedded-opentype'),
        url(../fonts/apercu_medium/apercu-medium.woff) format('woff'),
        url(../fonts/apercu_medium/apercu-medium.woff2) format('woff2'),
        url(../fonts/apercu_medium/apercu-medium.ttf) format('truetype');
    font-weight: 200;
    font-style: normal;
  }
  
  @font-face {
    font-family: 'apercu';
    src: url(../fonts/apercu_mono_regular/apercu-mono-regular.eot);
    src: url(../fonts/apercu_mono_regular/apercu-mono-regular.eot?#iefix) format('embedded-opentype'),
        url(../fonts/apercu_mono_regular/apercu-mono-regular.woff) format('woff'),
        url(../fonts/apercu_mono_regular/apercu-mono-regular.woff2) format('woff2'),
        url(../fonts/apercu_mono_regular/apercu-mono-regular.ttf) format('truetype');
    font-weight: 300;
    font-style: normal;
  }
  
  @font-face {
    font-family: 'apercu';
    src: url(../fonts/apercu_regular/apercu-regular.eot);
    src: url(../fonts/apercu_regular/apercu-regular.eot?#iefix}) format('embedded-opentype'),
        url(../fonts/apercu_regular/apercu-regular.woff) format('woff'),
        url(../fonts/apercu_regular/apercu-regular.woff2) format('woff2'),
        url(../fonts/apercu_regular/apercu-regular.ttf) format('truetype');
    font-weight: normal;
    font-style: normal;
  }