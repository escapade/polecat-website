import React from 'react'
import ReactPlayer from 'react-player'

import style from '../style.module.css'

class VideoPage extends React.Component {
  render() {
    return (

      <div className={style.page}>
        <div className={style.contentRowBlue}>
          <div className={style.videoWrapper}>
            <div className={style.aspectRatio} />
            <div className={style.video}>
              <ReactPlayer url='https://polecat.wistia.com/medias/n6v5onnu22' width='100%' height='100%'/>
            </div>
          </div>
        </div>
      </div>
     
    )
  }
}

export default VideoPage
