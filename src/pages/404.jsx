import React from 'react'
import cn from 'classnames'

import style from './style.module.css'

import Headline from '../components/Headline/index';
import Footer from '../components/Footer/Footer';
import BodyCopy from '../components/BodyCopy/index';

class notFound extends React.Component {
  render() {
    return (
      <div className={style.minHeightPage}>

        <div className={cn(style.contentRow ,style.contentRowBluePink)}>
          <div className={style.innerWrapper}>
            <Headline>404</Headline>
            <BodyCopy><p>Sorry. The page you're looking for couldn't be found.</p></BodyCopy>
          </div>
        </div>
        
        <Footer />
      </div>
    )
  }
}

export default notFound
