import React from 'react'
import Helmet from 'react-helmet'
import { graphql } from 'gatsby'
// import PropTypes from 'prop-types'
import cn from 'classnames'
import Markdown from 'markdown-to-jsx';

import config from '../../../data/SiteConfig'
import Layout from '../../components/Layout';

import ReactPlayer from 'react-player'

import style from '../style.module.css'
import btn from '../../components/Button/style.module.css'

import Button from '../../components/Button/index';
import Hero from '../../components/Hero/index';
import Subheadline from '../../components/Subheadline/index';
import Footer from '../../components/Footer/Footer';

import FeatureGrid from '../../components/FeatureGrid/index';
import FeatureGridStyle from '../../components/FeatureGrid/style.module.css';
import Feature from '../../components/Feature/index';
import featureStyle from '../../components/Feature/style.module.css';

import PeopleGrid from '../../components/PeopleGrid/index';
import PeopleGridStyle from '../../components/PeopleGrid/style.module.css';
import Person from '../../components/Person/index';
import History from '../../components/History/index';
import LogoLinks from '../../components/LogoLinks/index';
import CTA from '../../components/CTA/index';

class AboutPage extends React.Component {
  // static propTypes = {
  //   data: PropTypes.shape({
  //     markdownRemark: PropTypes.shape({
  //       html: PropTypes.string,
  //       frontmatter: PropTypes.shape({
  //         title: PropTypes.string,
  //       }),
  //     }),
  //   }).isRequired,
  // }

  render() {
    const page = this.props.data.page.frontmatter
    const fields = this.props.data.page.fields
    const dateEdges = this.props.data.dates.edges
    const peopleEdges = this.props.data.people.edges

    const metaDescription = (page.meta ? page.meta : config.siteDescription)

    return (
      <Layout>
        <Helmet>
          <title>{page.title} | {config.siteTitle}</title>
          <meta name="description" content={metaDescription}></meta>
        </Helmet>

        <div className={style.page}>
          <Hero title={page.hero.title} link={page.hero.link} linkText={page.hero.linkText} className={style.blue_purple_bk2} imageURL={page.hero.image}>
            <Markdown>{page.hero.copy}</Markdown>
          </Hero>

          {page.feature_grid && <div className={style.contentRow}>
            <div className={style.innerWrapper}>
              {page.feature_grid.title && <Subheadline>{page.feature_grid.title}</Subheadline>}
              {/* {page.feature_grid.description && <BodyCopy>{page.feature_grid.description}</BodyCopy>} */}

              <FeatureGrid className={FeatureGridStyle.twobytwo}>
                {page.feature_grid.feature.map((edge, i) => (
                  <Feature key={i} className={featureStyle.small}>
                    {edge.title && <h3>{edge.title}</h3>}
                    {edge.copy && <Markdown>{edge.copy}</Markdown>}
                  </Feature>
                ))}
              </FeatureGrid>
            </div>
          </div> }

          {page.videorow && <div className={style.contentRow}>
            <div className={cn(style.videoWrapper,style.paddingBottom)}>
              {page.videorow.title && <Subheadline>{page.videorow.title}</Subheadline>}
              <div className={style.aspectRatio} />
              <div className={style.video}>
                <ReactPlayer url={page.videorow.video} width='100%' height='100%'/>
              </div>
            </div>
          </div>}

          <div className={style.contentRowBlue}>
            <div className={style.sliderInnerWrapper}>
              {page.history && page.history.title && <Subheadline>{page.history.title}</Subheadline>}
              <History dateEdges={dateEdges} />
            </div>
          </div>

          <div className={style.contentRow}>
            <div className={style.innerWrapper}>
              {page.people && page.people.title && <Subheadline>{page.people.title}</Subheadline>}
              <PeopleGrid>

                {peopleEdges.map((edge, i) => (
                  <Person
                    key={i}
                    name={edge.node.frontmatter.title}
                    position={edge.node.frontmatter.jobtitle}
                    imageURL={edge.node.frontmatter.portrait}>
                    {edge.node.html}
                  </Person>
                ))}

                <div className={cn(PeopleGridStyle.gridAction, style.contentRowBluePurple)}>
                  <Subheadline>Join the Team</Subheadline>
                  <Button url="mailto:recruitment@polecat.com" className={btn.pink}>Find out More</Button>
                </div>
                <div className={cn(PeopleGridStyle.gridAction, style.contentRowGrey)}>
                  <Subheadline>Connect with Us</Subheadline>
                  <LogoLinks />
                </div>
              </PeopleGrid>
            </div>
          </div>

          {fields && fields.cta && <CTA data={fields.cta.frontmatter}/>}
          
          <Footer />
        </div>
      </Layout>
    )
  }
}

export default AboutPage

export const pageQuery = graphql`
  query AboutPageQuery {
    page: markdownRemark(fileAbsolutePath: { glob: "**/about.md" }) {
      frontmatter {
        title
        meta
				hero {
          title
          copy
          link
          linkText
          image
        }
        videorow {
          video
        }
        feature_grid {
          title
          feature {
            copy
            title
          }
        }
        history {
          title
        }
        people {
          title
        }
      }
      fields {
        cta {
          frontmatter {
            title
            calltoaction
            url
          }
        }
      }
    }

    dates: allMarkdownRemark(
      limit: 2000
      sort: { fields: [frontmatter___title] }
      filter: {
        frontmatter: {
          datepost: { eq: true }
        }
      }
    ) {
      edges {
        node {
          frontmatter {
            title
          }
          html
        }
      }
    }

    people: allMarkdownRemark(
      limit: 2000
      sort: { fields: [frontmatter___order] }
      filter: {
        frontmatter: {
          personpost: { eq: true }
          portrait: { ne: null }
          gridshow: { eq: true }
        }
      }
    ) {
      edges {
        node {
          frontmatter {
            title
            jobtitle
            portrait
            order
          }
          html
        }
      }
    }
  }
`
