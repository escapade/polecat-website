import React from 'react'
import Helmet from 'react-helmet'
import { graphql } from 'gatsby'
import cn from 'classnames'
import Markdown from 'markdown-to-jsx';
import { parse } from 'qs'

import config from '../../data/SiteConfig'
import Layout from '../components/Layout';

import style from './style.module.css'
import Modal from '../components/Modal'
import Hero from '../components/Hero/index'
import BodyCopy from '../components/BodyCopy/index'
import Subheadline from '../components/Subheadline/index'
import SecondaryButton from '../components/SecondaryButton/index'
import Footer from '../components/Footer/Footer'
import Product from '../components/Product/index'
import Clients from '../components/Clients/index'
import RegisterForm from '../components/RegisterForm/index'
import RegisterFormStyle from '../components/RegisterForm/style.module.css'
import Headline from '../components/Headline/index';
import ResourceGrid from '../components/ResourceGrid/index';
import ResourceGridStyle from '../components/ResourceGrid/style.module.css';
import Resource from '../components/Resource/index';

class Index extends React.Component {
  render() {
    const queryParams = parse(this.props.location.search, {
      ignoreQueryPrefix: true,
    })

    const closeModal = () => {
      this.props.history.replace({
        pathname: this.props.location.pathname,
        search: '',
      })
    }

    const showRegSuccessModal = queryParams.reg_success === 'true'
    const showRegFailModal = queryParams.reg_success === 'false'

    const page = this.props.data.page.frontmatter

    const latest_post = this.props.data.latestPost
    const latest_resource = this.props.data.latestResource
    const featured_post = this.props.data.featuredPost
    const featured_resource = this.props.data.featuredResource

    const metaDescription = (page.meta ? page.meta : config.siteDescription)

    return (
      <Layout>
        <Helmet>
          <title>{page.title} | {config.siteTitle}</title>
          <meta name="description" content={metaDescription}></meta>
        </Helmet>
        <div className={style.page}>
            <Hero title={page.hero.title} link={page.hero.link} linkText={page.hero.linkText} className={style.blue_purple_bk2} imageURL={page.hero.image}>
              <Markdown>{page.hero.copy}</Markdown>
            </Hero>

            <div className={style.contentRow}>
              <div className={style.innerWrapper}>
                {page.textrow.title && <Subheadline>{page.textrow.title}</Subheadline>}
                <BodyCopy className={cn(style.bodyCopy)} >
                  <Markdown>{page.textrow.copy}</Markdown>
                </BodyCopy>
              </div>
            </div>

            <div className={cn(style.contentRowBlue, style.PinkDashes)}>
              <div className={style.innerWrapper}>
                {page.products.title && <Subheadline>{page.products.title}</Subheadline>}
                {page.products.description && <BodyCopy>{page.products.description}</BodyCopy>}

                {page.products.product.map((edge, i) => (
                  <Product key={i} className={style.singleProduct}
                  title={edge.title}
                  link={edge.link}
                  linkText={edge.linkText}>
                    <Markdown>{edge.copy}</Markdown>
                  </Product>
                ))}
              </div>
            </div>

            <div className={style.contentRow}>
              <div className={style.innerWrapper}>
                {page.featured_content && page.featured_content.featured_title && <Subheadline>{page.featured_content.featured_title}</Subheadline>}

                  {page.featured_content.featured_post}<br/>
                  {page.featured_content.featured_resource}

                  <ResourceGrid isMasonry={false} className={ResourceGridStyle.squares}>
                    
                    {featured_post ?
                      featured_post.edges.map((edge, i) => (
                        <Resource key={i}
                          resourceType="featured_post"
                          title={edge.node.frontmatter.title}
                          subtitle={edge.node.frontmatter.date}
                          children={edge.node.excerpt}
                          linkURL={edge.node.fields.slug}
                          linkText="Read more"
                          imageURL={edge.node.frontmatter.cover}
                          companyName=""
                          companyLogo=""
                          download=""
                          target="_self"
                        />
                      ))
                    :
                      latest_post.edges.map((edge, i) => (
                        <Resource key={i}
                          resourceType="latest_post"
                          title={edge.node.frontmatter.title}
                          subtitle={edge.node.frontmatter.date}
                          children={edge.node.excerpt}
                          linkURL={edge.node.fields.slug}
                          linkText="Read more"
                          imageURL={edge.node.frontmatter.cover}
                          companyName=""
                          companyLogo=""
                          download=""
                          target="_self"
                        />
                      ))
                    }
                    
                    {featured_resource ? 
                      featured_resource.edges.map((edge, i) => (
                        <Resource key={i}
                          resourceType="featured_resource"
                          title={edge.node.frontmatter.title}
                          subtitle={edge.node.frontmatter.subtitle}
                          children={edge.node.excerpt}
                          linkURL={edge.node.frontmatter.link}
                          linkText={edge.node.frontmatter.linkText}
                          imageURL={edge.node.frontmatter.cover}
                          companyName={edge.node.frontmatter.company}
                          companyLogo={edge.node.frontmatter.logo}
                          download={edge.node.frontmatter.file}
                        />
                      ))
                    :
                      latest_resource.edges.map((edge, i) => (
                        <Resource key={i}
                          resourceType="latest_resource"
                          title={edge.node.frontmatter.title}
                          subtitle={edge.node.frontmatter.subtitle}
                          children={edge.node.excerpt}
                          linkURL={edge.node.frontmatter.link}
                          linkText={edge.node.frontmatter.linkText}
                          imageURL={edge.node.frontmatter.cover}
                          companyName={edge.node.frontmatter.company}
                          companyLogo={edge.node.frontmatter.logo}
                          download={edge.node.frontmatter.file}
                        />
                      ))
                    }
                    
                  </ResourceGrid>
              </div>
            </div>

            <div className={style.contentRow}>
              <div className={cn(style.innerWrapper, style.flex, style.noPaddingTop)}>
                <Clients title={page.clients.title} clients={page.clients.client} />
                {page.register_form &&
          <       RegisterForm
                    className={RegisterFormStyle.blue_bk}
                    title={page.register_form.title}
                    submitURL={page.register_form.url}
                    buttonText={page.register_form.submitText}>
                    <p>{page.register_form.description}</p>
                  </RegisterForm>
                }
              </div>
            </div>

            <Footer />

            <Modal isOpen={showRegSuccessModal} onRequestClose={closeModal}>
              <Headline>Thank you</Headline>
              <BodyCopy><p>Your newsletter registration was successful.</p></BodyCopy>
              <SecondaryButton onClick={closeModal}>Close</SecondaryButton>
            </Modal>

            <Modal isOpen={showRegFailModal} onRequestClose={closeModal}>
              <Headline>Sorry</Headline>
              <BodyCopy><p>There was an error with your form.<br/>Please check you've completed all fields and try again.</p></BodyCopy>
              <SecondaryButton onClick={closeModal}>Close</SecondaryButton>
            </Modal>
          </div>
        </Layout>
    )
  }
}

export default Index

/* eslint no-undef: "off"*/
export const pageQuery = graphql`
  query HomePageQuery {
    page: markdownRemark(fileAbsolutePath: { glob: "**/home.md" }) {
      frontmatter {
        title
        meta
        hero {
          title
          copy
          link
          linkText
          image
        }
        textrow {
          title
          copy
        }
        products {
          title
          description
          product {
            title
            copy
            link
            linkText
          }
        }
        featured_content {
          featured_title
        }
        register_form {
          title
          description
          url
          submitText
        }
        clients {
          title
          client {
            logo
            name
          }
        }
      }
    }
    latestPost: allMarkdownRemark(
      limit: 1
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { 
        frontmatter: { 
          blogpost: { eq: true }
        } 
      }
    ) {
      edges {
        node {
          fields {
            slug
          }
          excerpt(pruneLength: 240)
          frontmatter {
            author
            title
            tags
            cover
            date(formatString: "MMMM DD, YYYY")
            published
          }
        }
      }
    }
    featuredPost: allMarkdownRemark(
      limit: 1
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { 
        frontmatter: { 
          blogpost: { eq: true }
          featured: { eq: true }          
        } 
      }
    ) {
      edges {
        node {
          fields {
            slug
          }
          excerpt(pruneLength: 240)
          frontmatter {
            author
            title
            tags
            cover
            date(formatString: "MMMM DD, YYYY")
            published
          }
        }
      }
    }
    latestResource: allMarkdownRemark(
      limit: 1
      sort: { fields: [frontmatter___date], order: DESC }
      filter: {
        frontmatter: {
          resourcepost: { eq: true }
          resourceType: { ne: "quotation" }
        }
      }
    ) {
      edges {
        node {
          fields {
            slug
          }
          frontmatter {
            resourceType
            date(formatString: "MMMM DD, YYYY")
            title
            subtitle
            tags
            cover
            company
            logo
            link
            linkText
            file
          }
          excerpt(pruneLength: 240)
        }
      }
    }
    featuredResource: allMarkdownRemark(
      limit: 1
      sort: { fields: [frontmatter___date], order: ASC }
      filter: {
        frontmatter: {
          resourcepost: { eq: true }
          featured: { eq: true }
          resourceType: { ne: "quotation" }
        }
      }
    ) {
      edges {
        node {
          fields {
            slug
          }
          frontmatter {
            resourceType
            date(formatString: "MMMM DD, YYYY")
            title
            subtitle
            tags
            cover
            company
            logo
            link
            linkText
            file
          }
          excerpt(pruneLength: 240)
        }
      }
    }
  }
`