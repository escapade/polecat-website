import React from 'react'
import Helmet from 'react-helmet'
import { graphql } from 'gatsby'
// import PropTypes from 'prop-types'
import cn from 'classnames'
import { parse } from 'qs'
import Markdown from 'markdown-to-jsx';

import config from '../../../data/SiteConfig' 
import Layout from '../../components/Layout';

import Modal from '../../components/Modal'
import contactStyle from './style.module.css'
import style from '../style.module.css'

import Headline from '../../components/Headline/index';
import Subheadline from '../../components/Subheadline/index';
import Footer from '../../components/Footer/Footer';
import ContactForm from '../../components/ContactForm/index';
import BodyCopy from '../../components/BodyCopy/index';
import BodyCopyStyle from '../../components/BodyCopy/style.module.css';
import OfficeGrid from '../../components/OfficeGrid/index';
import Office from '../../components/Office/index';
import SecondaryButton from '../../components/SecondaryButton/index';

import CTA from '../../components/CTA/index';

class ContactPage extends React.Component {
  // static propTypes = {
  //   data: PropTypes.shape({
  //     markdownRemark: PropTypes.shape({
  //       html: PropTypes.string,
  //       frontmatter: PropTypes.shape({
  //         title: PropTypes.string,
  //       }),
  //     }),
  //   }).isRequired,
  // }

  render() {
    const queryParams = parse(this.props.location.search, {
      ignoreQueryPrefix: true,
    })

    const closeModal = () => {
      this.props.history.replace({
        pathname: this.props.location.pathname,
        search: '',
      })
    }

    const showContactSuccessModal = queryParams.contact_success === 'true'
    const showContactFailModal = queryParams.contact_success === 'false'

    const page = this.props.data.page.frontmatter
    const fields = this.props.data.page.fields
    const locations = this.props.data.location.edges
    const metaDescription = (page.meta ? page.meta : config.siteDescription)
  
    return (
      <Layout>
        <Helmet>
          <title>{page.title} | {config.siteTitle}</title>
          <meta name="description" content={metaDescription}></meta>
        </Helmet>
        <div className={style.page}>

          <div className={cn(style.contentRowBluePink, style.halfHeightBk)}>
            <div className={contactStyle.contactWrapper}>
            {page.hero.title && <Headline>{page.hero.title}</Headline>}
              <div className={contactStyle.contactInnerWrapper}>
                {page.form.title && <Subheadline>{page.form.title}</Subheadline>}
                <div className={contactStyle.flex}>
                  <div className={contactStyle.image} />
                  <ContactForm title="" submitURL="https://info.polecat.com/l/479701/2018-06-25/6zpjq" buttonText="Register">
                    <Markdown>{page.form.copy}</Markdown>
                  </ContactForm>
                </div>
              </div>
            </div>
          </div>

          <div className={style.contentRow}>
            <div className={style.innerWrapper}>
              {page.textrow.title && <Subheadline>{page.textrow.title}</Subheadline>}
              <BodyCopy className={cn(BodyCopyStyle.largeLinks)} >
                <Markdown>{page.textrow.copy}</Markdown>
              </BodyCopy>
            </div>
          </div>

          <div className={style.contentRow}>
            <div className={style.innerWrapper}>
              <OfficeGrid>

                {locations.map((edge, i) => (
                  <Office
                    key={i}
                    title={edge.node.frontmatter.title} 
                    phone={edge.node.frontmatter.phone}
                    postcode={edge.node.frontmatter.postcode}
                    country={edge.node.frontmatter.country}>
                    <Markdown options={{ forceInline: true }}>{edge.node.frontmatter.address}</Markdown>
                  </Office>
                ))}
              </OfficeGrid>

            </div>
          </div>

          {fields && fields.cta && <CTA data={fields.cta.frontmatter}/>}
          
          <Footer />

          <Modal isOpen={showContactSuccessModal} onRequestClose={closeModal}>
            <Headline>Thank you</Headline>
            <BodyCopy><p>Your contact has been received. We will contact you shortly.</p></BodyCopy>
            <SecondaryButton onClick={closeModal}>Close</SecondaryButton>
          </Modal>

          <Modal isOpen={showContactFailModal} onRequestClose={closeModal}>
            <Headline>Sorry</Headline>
            <BodyCopy><p>There was an error with your form.<br/>Please check you've completed all fields and try again.</p></BodyCopy>
            <SecondaryButton onClick={closeModal}>Close</SecondaryButton>
          </Modal>
        </div>
        
      </Layout>  
    )
  }
}

export default ContactPage

/* eslint no-undef: "off"*/
export const pageQuery = graphql`
  query ContactPageQuery {
    page: markdownRemark(fileAbsolutePath: { glob: "**/contact.md" }) {
      frontmatter {
        title
        meta
        hero {
          title
          copy
        }
        form {
          title
          copy
        }
        textrow {
          title
          copy
        }
      }
      fields {
        cta {
          frontmatter {
            title
            calltoaction
            url
          }
        }
      }
    }
    location: allMarkdownRemark(
      limit: 2000
      sort: { fields: [frontmatter___order], order: ASC }
      filter: {
        frontmatter: {
          locationpost: { eq: true }
        }
      }
    ) {
      edges {
        node {
          frontmatter {
            title
            address
            country
            postcode
            phone
            order
          }
        }
      }
    }
  }
`