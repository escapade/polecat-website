import React from 'react'
import Helmet from 'react-helmet'
import { graphql } from 'gatsby'
// import PropTypes from 'prop-types'
import cn from 'classnames'
import { parse } from 'qs'

import config from '../../../data/SiteConfig' 
import Layout from '../../components/Layout';

import style from '../style.module.css'
import Modal from '../../components/Modal'
import ModalStyle from '../../components/Modal/style.module.css'
import heroStyle from '../../components/Hero/style.module.css'

import Button from '../../components/Button/index'
import SecondaryButton from '../../components/SecondaryButton/index'
import Headline from '../../components/Headline/index'
import BodyCopy from '../../components/BodyCopy/index'

import StoryGrid from '../../components/StoryGrid/index';
import Footer from '../../components/Footer/Footer';

import PostListing from '../../components/PostListing'

import RegisterForm from '../../components/RegisterForm/index'
import RegisterFormStyle from '../../components/RegisterForm/style.module.css'

import CTA from '../../components/CTA/index';

export default class Index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {showRegModal: false};

    this.toggleRegModal = this.toggleRegModal.bind(this)
  }

  toggleRegModal(event) {
    this.setState({showRegModal: !this.state.showRegModal});
  }

  render() {
    const page = this.props.data.page.frontmatter
    const fields = this.props.data.page.fields
    const posts = this.props.data.posts.edges
    
    const queryParams = parse(this.props.location.search, {
      ignoreQueryPrefix: true,
    })

    const closeRegModal = () => {
      this.setState({showRegModal: false});
    }
    
    const closeModal = () => {
      this.props.history.replace({
        pathname: this.props.location.pathname,
        search: '',
      })
    }

    const showRegSuccessModal = queryParams.reg_success === 'true'
    const showRegFailModal = queryParams.reg_success === 'false'

    const metaDescription = (page.meta ? page.meta : config.siteDescription)
    const siteUrl = config.siteUrl
    const stagingUrl = config.stagingUrl

    return (
      <Layout>
        <Helmet>
          <title>{page.title} | {config.siteTitle}</title>
          <meta name="description" content={metaDescription}></meta>
        </Helmet>
        <div className={style.page}>
          <div className={style.contentRowBluePink}>
            <div className={cn(heroStyle.small, heroStyle.hasButton)}>
              <Headline>{page.hero.title}</Headline>
              <BodyCopy>{page.hero.copy}</BodyCopy>
              <Button onClick={this.toggleRegModal}>Subscribe</Button>
            </div>
          </div>

          <div className={style.contentRow}>
            <div className={style.innerWrapper}>
              <StoryGrid>              
                <React.Fragment>
                  {/* <SEO postEdges={postEdges} /> */}
                  {posts.map((edge, i) => (
                    siteUrl == stagingUrl || edge.node.frontmatter.published != false ?
                      <PostListing
                        key={i}
                        slug={edge.node.fields.slug}
                        backgroundImage={edge.node.frontmatter.cover}
                        excerpt={edge.node.excerpt}
                        title={edge.node.frontmatter.title}
                        date={edge.node.frontmatter.date}
                      />
                    : ""
                  ))}
                </React.Fragment>
              </StoryGrid>
            </div>
          </div>

          {fields && fields.cta && <CTA data={fields.cta.frontmatter}/>}
          
          <Footer />

          <Modal className={ModalStyle.noStyle} isOpen={this.state.showRegModal} onRequestClose={closeRegModal}>
            <RegisterForm 
                className={RegisterFormStyle.blue_bk}
              title="Subscribe to the Polecat Blog"
              submitURL="https://info.polecat.com/l/479701/2018-07-25/9symn"
              buttonText="Subscribe">
            </RegisterForm>
            {/* <SecondaryButton onClick={closeModal}>Close</SecondaryButton> */}
          </Modal>

          <Modal isOpen={showRegSuccessModal} onRequestClose={closeModal}>
            <Headline>Thank you</Headline>
            <BodyCopy><p>Your newsletter registration was successful.</p></BodyCopy>
            <SecondaryButton onClick={closeModal}>Close</SecondaryButton>
          </Modal>

          <Modal isOpen={showRegFailModal} onRequestClose={closeModal}>
            <Headline>Sorry</Headline>
            <BodyCopy><p>There was an error with your form.<br/>Please check you've completed all fields and try again.</p></BodyCopy>
            <SecondaryButton onClick={closeModal}>Close</SecondaryButton>
          </Modal>
        </div>
      </Layout>
    )
  }
}

/* eslint no-undef: "off"*/
export const pageQuery = graphql`
  query BlogPageQuery {
    page: markdownRemark(fileAbsolutePath: { glob: "**/blog.md" }) {
      frontmatter {
        title
        meta
        hero {
          title
          copy
        }
      }
      fields {
        cta {
          frontmatter {
            title
            calltoaction
            url
          }
        }
      }
    }
    posts: allMarkdownRemark(
      limit: 2000
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { 
        frontmatter: { 
          blogpost: { eq: true }
        } 
      }
    ) {
      edges {
        node {
          fields {
            slug
          }
          excerpt(pruneLength: 240)
          frontmatter {
            author
            title
            tags
            cover
            date(formatString: "MMMM DD, YYYY")
            published
          }
        }
      }
    }
  }
`