import React from 'react'

import Layout from '../../components/Layout';

import style from '../style.module.css';
import Footer from '../../components/Footer/Footer';
import WorkableAPI from '../../components/WorkableAPI/index';

class WorkablePage extends React.Component {
  render() {
    return (
      <Layout>
        <div className={style.page}>

          <div className={style.contentRow}>
            <div className={style.innerWrapper}>
              <WorkableAPI/>
            </div>
          </div>

          <Footer />
        </div>
      </Layout>
    )
  }
}

export default WorkablePage