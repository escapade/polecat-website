import React from 'react'
import Helmet from 'react-helmet'
import { graphql } from 'gatsby'
// import PropTypes from 'prop-types'
import cn from 'classnames'
import Markdown from 'markdown-to-jsx';

import config from '../../../data/SiteConfig' 
import Layout from '../../components/Layout';

import style from '../style.module.css'
import heroStyle from '../../components/Hero/style.module.css'
import Headline from '../../components/Headline/index';
import Footer from '../../components/Footer/Footer';
import BodyCopy from '../../components/BodyCopy/index';
import BodyCopyStyle from '../../components/BodyCopy/style.module.css';

class PrivacyPage extends React.Component {
  // static propTypes = {
  //   data: PropTypes.shape({
  //     markdownRemark: PropTypes.shape({
  //       html: PropTypes.string,
  //       frontmatter: PropTypes.shape({
  //         title: PropTypes.string,
  //       }),
  //     }),
  //   }).isRequired,
  // }

  render() {
    const page = this.props.data.page.frontmatter
    const html = this.props.data.page.html
    const excerpt = this.props.data.page.excerpt
    const metaDescription = (page.meta ? page.meta : excerpt)

    return (
      <Layout>
        <Helmet>
          <title>{page.title} | {config.siteTitle}</title>
          <meta name="description" content={metaDescription}></meta>
        </Helmet>
        <div className={style.page}>
          <div className={style.contentRowBluePurple2}>
            <div className={cn(heroStyle.small, heroStyle.centre)}>
              <Headline>{page.hero.title}</Headline>
            </div>
          </div>

          <div className={style.contentRow}>
            <div className={style.innerWrapper}>
              <BodyCopy className={cn(style.bodyCopy, BodyCopyStyle.leftAlign)} >
                <Markdown>{html}</Markdown>
              </BodyCopy>
            </div>
          </div>
        
          <Footer />
        </div>
      </Layout>
    )
  }
}

export default PrivacyPage

/* eslint no-undef: "off"*/
export const pageQuery = graphql`
  query PrivacyQuery {
    page: markdownRemark(fileAbsolutePath: { glob: "**/privacy.md" }) {
      frontmatter {
        title
        meta
        hero {
          title
          copy
        }
      }
      html
      excerpt(pruneLength: 155)
    }
  }
`