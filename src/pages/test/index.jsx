import React from 'react'
import Helmet from 'react-helmet'
import { graphql } from 'gatsby'
import cn from 'classnames'

import config from '../../../data/SiteConfig'
import Layout from '../../components/Layout';

import style from '../style.module.css'
import heroStyle from '../../components/Hero/style.module.css'
import Headline from '../../components/Headline/index';
import Footer from '../../components/Footer/Footer';
import BodyCopy from '../../components/BodyCopy/index';
import BodyCopyStyle from '../../components/BodyCopy/style.module.css';
import mdCTA from '../../components/markdownCTA/index';

import rehypeReact from "rehype-react"

class TestPage extends React.Component {
  render() {
    const page = this.props.data.page.frontmatter
    const fields = this.props.data.page.fields
    // const html = this.props.data.page.html
    const htmlAst = this.props.data.page.htmlAst
    const excerpt = this.props.data.page.excerpt
    const metaDescription = (page.meta ? page.meta : excerpt)

    const renderAst = new rehypeReact({
      createElement: React.createElement,
      components: { "call-to-action": mdCTA },
    }).Compiler

    return (
      <Layout>
        <Helmet>
          <title>{page.title} | {config.siteTitle}</title>
          <meta name="description" content={metaDescription}></meta>
        </Helmet>
        <div className={style.page}>
          <div className={style.contentRowBluePurple2}>
            <div className={cn(heroStyle.small, heroStyle.centre)}>
              <Headline>{page.title}</Headline>
            </div>
          </div>

          <div className={style.contentRow}>
            <div className={style.innerWrapper}>
              <BodyCopy className={cn(style.bodyCopy, BodyCopyStyle.leftAlign)} >
                {/* <Markdown>{html}</Markdown> */}
                {renderAst(htmlAst)}
              </BodyCopy>
            </div>
          </div>

          {/* {fields && fields.cta && <CTA data={fields.cta.frontmatter}/>} */}
        
          <Footer />
        </div>
      </Layout>
    )
  }
}

export default TestPage

/* eslint no-undef: "off"*/
export const pageQuery = graphql`
  query TestQuery {
    page: markdownRemark(fileAbsolutePath: { glob: "**/test.md" }) {
      frontmatter {
        title
        meta
      }
      htmlAst
    }
    ctas: allMarkdownRemark(
      limit: 2000
      sort: { fields: [frontmatter___order] }
      filter: {
        frontmatter: {
          ctapost: { eq: true }
        }
      }
    ) {
      edges {
        node {
          frontmatter {
            title
            calltoaction
            url
          }
        }
      }
    }
  }
`