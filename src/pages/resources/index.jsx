import React from 'react'
import Helmet from 'react-helmet'
import { graphql } from 'gatsby'
// import PropTypes from 'prop-types'

import config from '../../../data/SiteConfig' 
import Layout from '../../components/Layout';

import style from '../style.module.css'
import heroStyle from '../../components/Hero/style.module.css'
import Headline from '../../components/Headline/index';
import BodyCopy from '../../components/BodyCopy/index';
import Footer from '../../components/Footer/Footer';
import ResourceGrid from '../../components/ResourceGrid/index';

import Resource from '../../components/Resource/index';

import SEO from '../../components/SEO/SEO'

import CTA from '../../components/CTA/index';

export default class ResourcesPage extends React.Component {
  constructor(props) {
    super(props);
  }

  // static propTypes = {
  //   data: PropTypes.shape({
  //     markdownRemark: PropTypes.shape({
  //       html: PropTypes.string,
  //       frontmatter: PropTypes.shape({
  //         title: PropTypes.string,
  //       }),
  //     }),
  //   }).isRequired,
  // }

  render() {
    const page = this.props.data.page.frontmatter
    const fields = this.props.data.page.fields
    const resources = this.props.data.resources.edges

    const metaDescription = (page.meta ? page.meta : config.siteDescription)

    return (
      <Layout>
        <Helmet>
          <title>{page.title} | {config.siteTitle}</title>
          <meta name="description" content={metaDescription}></meta>
        </Helmet>
        <div className={style.page}>
          <div className={style.contentRowBluePink}>
            <div className={heroStyle.small}>
              <Headline>{page.hero.title}</Headline>
              <BodyCopy>{page.hero.copy}</BodyCopy>
            </div>
          </div>

          <div className={style.contentRow}>
            <div className={style.innerWrapper}>
              <ResourceGrid>
                {resources.map((edge, i) => (
                  <React.Fragment key={i}>
                    <SEO postEdges={resources} />
                    <Resource
                      resourceType={edge.node.frontmatter.resourceType}
                      title={edge.node.frontmatter.title}
                      subtitle={edge.node.frontmatter.subtitle}
                      children={edge.node.excerpt}
                      slug={edge.node.fields.slug}
                      linkURL={edge.node.frontmatter.link}
                      linkText={edge.node.frontmatter.linkText}
                      imageURL={edge.node.frontmatter.cover}
                      companyName={edge.node.frontmatter.company}
                      companyLogo={edge.node.frontmatter.logo}
                      download={edge.node.frontmatter.file}
                    />
                  </React.Fragment>
                ))}
              </ResourceGrid>
            </div>
          </div>

          {fields && fields.cta && <CTA data={fields.cta.frontmatter}/>}
          
          <Footer />
        </div>
      </Layout>
    )
  }
}

/* eslint no-undef: "off"*/
export const pageQuery = graphql`
  query ResourcePageQuery {
    page: markdownRemark(fileAbsolutePath: { glob: "**/resources.md" }) {
      frontmatter {
        title
        meta
        hero {
          title
          copy
        }
      }
      fields {
        cta {
          frontmatter {
            title
            calltoaction
            url
          }
        }
      }
    }
    resources: allMarkdownRemark(
      limit: 2000
      sort: { fields: [frontmatter___date], order: DESC }
      filter: {
        frontmatter: {
          resourcepost: { eq: true }
        }
      }
    ) {
      edges {
        node {
          fields {
            slug
          }
          frontmatter {
            resourceType
            date(formatString: "MMMM DD, YYYY")
            title
            subtitle
            tags
            cover
            company
            logo
            link
            linkText
            file
          }
          excerpt(pruneLength: 650)
        }
      }
    }
  }
`