import React from 'react'
import Helmet from 'react-helmet'
import { graphql } from 'gatsby'
// import PropTypes from 'prop-types'
import cn from 'classnames'
import Markdown from 'markdown-to-jsx';

import config from '../../../data/SiteConfig' 
import Layout from '../../components/Layout';

import style from '../style.module.css'

import Hero from '../../components/Hero/index';
import Subheadline from '../../components/Subheadline/index';
import Footer from '../../components/Footer/Footer';
import FeatureGrid from '../../components/FeatureGrid/index';
import FeatureGridStyle from '../../components/FeatureGrid/style.module.css';
import Feature from '../../components/Feature/index';
import FeatureStyle from '../../components/Feature/style.module.css';

import CTA from '../../components/CTA/index';

class ApproachPage extends React.Component {
  // static propTypes = {
  //   data: PropTypes.shape({
  //     markdownRemark: PropTypes.shape({
  //       html: PropTypes.string,
  //       frontmatter: PropTypes.shape({
  //         title: PropTypes.string,
  //       }),
  //     }),
  //   }).isRequired,
  // }

  render() {
    const page = this.props.data.page.frontmatter
    const fields = this.props.data.page.fields    

    const metaDescription = (page.meta ? page.meta : config.siteDescription)

    return (
      <Layout>
        <Helmet>
          <title>{page.title} | {config.siteTitle}</title>
          <meta name="description" content={metaDescription}></meta>
        </Helmet>
        <div className={style.page}>
          <Hero title={page.hero.title} link={page.hero.link} linkText={page.hero.linkText} className={style.blue_purple_bk2} imageURL={page.hero.image}>
            <Markdown>{page.hero.copy}</Markdown>
          </Hero>

          {page.feature_grid && <div className={style.contentRow}>
            <div className={style.innerWrapper}>
              {page.feature_grid.title && <Subheadline>{page.feature_grid.title}</Subheadline>}
              {/* {page.feature_grid.description && <BodyCopy>{page.feature_grid.description}</BodyCopy>} */}

              <FeatureGrid className={FeatureGridStyle.threebythree}>
                {page.feature_grid.feature.map((edge, i) => (
                  <Feature key={i} className={FeatureStyle.small} title={edge.title}>
                    {edge.copy && <Markdown>{edge.copy}</Markdown>}
                  </Feature>
                ))}
              </FeatureGrid>
            </div>
          </div>}

          {page.imagerow && <div className={cn(style.contentRow, style.contentRowGrey2)}>
            <div className={style.innerWrapper}>
              {page.imagerow.title && <Subheadline>{page.imagerow.title}</Subheadline>}
              <img src={page.imagerow.image} />
            </div>
          </div>}

          {page.feature_grid2 && <div className={style.contentRow}>
            <div className={style.innerWrapper}>
              {page.feature_grid2.title && <Subheadline>{page.feature_grid2.title}</Subheadline>}
              {/* {page.feature_grid.description && <BodyCopy>{page.feature_grid.description}</BodyCopy>} */}

              <FeatureGrid className={FeatureGridStyle.threebythree}>
                {page.feature_grid2.feature.map((edge, i) => (
                  <Feature key={i} iconURL={edge.icon} title={edge.title}>
                    {edge.copy && <Markdown>{edge.copy}</Markdown>}
                  </Feature>
                ))}
              </FeatureGrid>
            </div>
          </div>}

          {fields && fields.cta && <CTA data={fields.cta.frontmatter}/>}
          
          <Footer />
        </div>
      </Layout>
    )
  }
}

export default ApproachPage

/* eslint no-undef: "off"*/
export const pageQuery = graphql`
  query ApproachPageQuery {
    page: markdownRemark(fileAbsolutePath: { glob: "**/approach.md" }) {
      frontmatter {
        title
        meta
        hero {
          title
          copy
          link
          linkText
          image
        }
        feature_grid {
          title
          
          feature {
            copy
            icon
            title
          }
        }
        imagerow {
          title
          image
        }
        feature_grid2 {
          title
          
          feature {
            copy
            icon
            title
          }
        }
      }
      fields {
        cta {
          frontmatter {
            title
            calltoaction
            url
          }
        }
      }
    }
  }
`
