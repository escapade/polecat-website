import React from 'react'
import Helmet from 'react-helmet'
import { graphql } from 'gatsby'
import Markdown from 'markdown-to-jsx';

import config from '../../../data/SiteConfig' 
import Layout from '../../components/Layout';

import style from '../style.module.css'
import productStyle from './style.module.css'
import Hero from '../../components/Hero/index';
import Subheadline from '../../components/Subheadline/index';
import Footer from '../../components/Footer/Footer';
import FeatureGrid from '../../components/FeatureGrid/index';
import featureGridStyle from '../../components/FeatureGrid/style.module.css';
import Feature from '../../components/Feature/index';
import FeatureStyle from '../../components/Feature/style.module.css';

import KeyPoints from '../../components/KeyPoints/index';
import CTA from '../../components/CTA/index';


class ProductPage extends React.Component {
  // static propTypes = {
  //   data: PropTypes.shape({
  //     markdownRemark: PropTypes.shape({
  //       html: PropTypes.string,
  //       frontmatter: PropTypes.shape({
  //         title: PropTypes.string,
  //       }),
  //     }),
  //   }).isRequired,
  // }

  render() {
    const page = this.props.data.page.frontmatter
    const fields = this.props.data.page.fields

    const metaDescription = (page.meta ? page.meta : config.siteDescription)

    return (
      <Layout>
        <Helmet>
          <title>{page.title} | {config.siteTitle}</title>
          <meta name="description" content={metaDescription}></meta>
        </Helmet>

        <div className={style.page}>
          <Hero title={page.hero.title} link={page.hero.link} linkText={page.hero.linkText} className={style.mint_blue_bk} imageURL={page.hero.image}>
            <Markdown>{page.hero.copy}</Markdown>
          </Hero>

          {page.feature_grid && <div className={style.contentRow}>
            <div className={style.innerWrapper}>
              {page.feature_grid.title && <Subheadline>{page.feature_grid.title}</Subheadline>}
              {/* {page.feature_grid.description && <BodyCopy>{page.feature_grid.description}</BodyCopy>} */}

              <FeatureGrid>
                {page.feature_grid.feature.map((edge, i) => (
                  <Feature key={i} className={FeatureStyle.small} title={edge.title}>
                    {edge.copy && <Markdown>{edge.copy}</Markdown>}
                  </Feature>
                ))}
              </FeatureGrid>
            </div>
          </div>}

          {page.keypoint_grid && <div className={style.contentRow}>
            <div className={productStyle.keyPointWrapper}>

                {page.keypoint_grid.keypoint_row.map((edge, i) => (                
                  <KeyPoints key={i} className={style.mint_blue_bk} imageURL={edge.image}>
                    <ol key={i}>
                      {edge.points.map((point, i) => (
                        <li key={i}>
                          {point.title && <h4>{point.title}</h4>}
                          {point.copy && point.copy}
                        </li>
                      ))}
                    </ol>
                  </KeyPoints>
                ))}
                
            </div>
          </div>}

          {page.feature_grid2 && <div className={style.contentRow}>
            <div className={style.innerWrapper}>
              {page.feature_grid2.title && <Subheadline>{page.feature_grid2.title}</Subheadline>}
              <FeatureGrid>
                {page.feature_grid2.feature.map((edge, i) => (
                  <React.Fragment key={i}>
                    <Feature iconURL={edge.icon} title={edge.title}>
                      <Markdown>{edge.copy}</Markdown>
                    </Feature>
                    {i == 2 &&
                      <div className={featureGridStyle.desktopBreak}/>       
                    }
                  </React.Fragment>
                ))}
              </FeatureGrid>
            </div>
          </div>}

          {fields && fields.cta && <CTA data={fields.cta.frontmatter}/>}
          
          <Footer />
        </div>
      </Layout>
    )
  }
}

export default ProductPage

/* eslint no-undef: "off"*/
export const pageQuery = graphql`
  query ProductPageQuery {
    page: markdownRemark(fileAbsolutePath: { glob: "**/repvault.md" }) {
      frontmatter {
        title
        meta
        hero {
          title
          copy
          link
          linkText
          image
        }
        feature_grid {
          title
          feature {
            copy
            icon
            title
          }
        }
        keypoint_grid {
          keypoint_row {
            image
            points {
              copy
              title
            }
          }
        }
        feature_grid2 {
          feature {
            copy
            icon
            title
          }
        }
      }
      fields {
        cta {
          frontmatter {
            title
            calltoaction
            url
          }
        }
      }
    }
  }
`
