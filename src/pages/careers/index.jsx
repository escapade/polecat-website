import React from 'react'
import Helmet from 'react-helmet'
import { graphql } from 'gatsby'
// import PropTypes from 'prop-types'
import cn from 'classnames'
import Markdown from 'markdown-to-jsx';

import config from '../../../data/SiteConfig' 
import Layout from '../../components/Layout';

// import Instafeed from 'react-instafeed';
import ReactPlayer from 'react-player'

import style from '../style.module.css';
import heroStyle from '../../components/Hero/style.module.css';
import Headline from '../../components/Headline/index';
import Subheadline from '../../components/Subheadline/index';
import Footer from '../../components/Footer/Footer';
import BodyCopy from '../../components/BodyCopy/index';
import FeatureGrid from '../../components/FeatureGrid/index';
import Feature from '../../components/Feature/index';
import Workable from '../../components/Workable/index';

import WorkableAPI from '../../components/WorkableAPI/index';

import CTA from '../../components/CTA/index';

class CareersPage extends React.Component {
  render() {
    const page = this.props.data.page.frontmatter
    const fields = this.props.data.page.fields

    const metaDescription = (page.meta ? page.meta : config.siteDescription)

    return (
      <Layout>
        <Helmet>
          <title>{page.title} | {config.siteTitle}</title>
          <meta name="description" content={metaDescription}></meta>
        </Helmet>
        <div className={style.page}>
          <div className={style.contentRowBluePurple2}>
            <div className={cn(heroStyle.small, heroStyle.centre)}>
              <Headline>{page.hero.title}</Headline>
            </div>
          </div>

          {page.textrow && <div className={style.contentRow}>
            <div className={style.innerWrapper}>
              {page.textrow.title && <Subheadline>{page.textrow.title}</Subheadline>}
              <BodyCopy className={cn(style.bodyCopy)} >
                <Markdown>{page.textrow.copy}</Markdown>
              </BodyCopy>
            </div>
          </div>}

          {page.videorow && <div className={cn(style.contentRowBlue, style.PinkDashes)}>
            <div className={style.videoWrapper}>
              {page.videorow.title && <Subheadline>{page.videorow.title}</Subheadline>}
              <div className={style.aspectRatio} />
              <div className={style.video}>
                <ReactPlayer url={page.videorow.video} width='100%' height='100%'/>
              </div>
            </div>
          </div>}

          {page.textrow2 && <div className={style.contentRow}>
            <div className={cn(style.innerWrapper, style.noPaddingBottom)}>
              {page.textrow2.title && <Subheadline>{page.textrow2.title}</Subheadline> }
              <BodyCopy className={cn(style.bodyCopy)} >
                <Markdown>
                  {page.textrow2.copy}
                </Markdown>
              </BodyCopy>
            </div>
          </div>}

          {page.feature_grid && <div className={style.contentRow}>
            <div className={style.innerWrapper}>
              {page.feature_grid.title && <Subheadline>{page.feature_grid.title}</Subheadline>}
              {/* {page.feature_grid.description && <BodyCopy>{page.feature_grid.description}</BodyCopy>} */}

              <FeatureGrid>
                {page.feature_grid.feature.map((edge, i) => (
                  <Feature key={i} iconURL={edge.icon} title={edge.title}>
                    {edge.copy && <Markdown>{edge.copy}</Markdown>}
                  </Feature>
                ))}
              </FeatureGrid>
            </div>
          </div>}

          {page.textrow3 && 
            <div className={style.contentRowBlue}>
              <div className={style.innerWrapper}>
                {page.textrow3.title && <Subheadline>{page.textrow3.title}</Subheadline>}
                <WorkableAPI/>
                {page.textrow3.copy && 
                <BodyCopy className={cn(style.bodyCopy)} >
                  <Markdown>{page.textrow3.copy}</Markdown>
                </BodyCopy>}
              </div>
            </div>
          }

          {fields && fields.cta && <CTA data={fields.cta.frontmatter}/>}
          
          <Footer />
        </div>
      </Layout>
    )
  }
}

export default CareersPage

/* eslint no-undef: "off"*/
export const pageQuery = graphql`
  query CareersPageQuery {
    page: markdownRemark(fileAbsolutePath: { glob: "**/careers.md" }) {
      frontmatter {
        title
        meta
        hero {
          title
          copy
        }
        textrow {
          title
          copy
        }
        videorow {
          video
        }
        textrow2 {
          title
          copy
        }
        feature_grid {
          title
          
          feature {
            copy
            icon
            title
          }
        }
        textrow3 {
          title
          copy
        }
      }
      fields {
        cta {
          frontmatter {
            title
            calltoaction
            url
          }
        }
      }
    }
  }
`