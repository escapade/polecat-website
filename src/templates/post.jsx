import React from 'react'
import Helmet from 'react-helmet'
import { graphql } from 'gatsby'
import Link from 'gatsby-link'
import cn from 'classnames'

import SEO from '../components/SEO/SEO'
import config from '../../data/SiteConfig'
import Layout from '../components/Layout';

import style from '../pages/style.module.css'
import heroStyle from '../components/Hero/style.module.css'
import postStyle from './post.module.css'
import Headline from '../components/Headline/index'
import HeadlineStyle from '../components/Headline/style.module.css'
import Button from '../components/Button/index'
import btn from '../components/Button/style.module.css'
import Footer from '../components/Footer/Footer'

import SocialLinks from '../components/SocialLinks/SocialLinks';

import mdCTA from '../components/markdownCTA/index';
import rehypeReact from "rehype-react"

export default class PostTemplate extends React.Component {
  constructor(props) {
    super(props);
    this.heroWrapper = React.createRef();
    this.contentWrapper = React.createRef();
    this.contentTitle = React.createRef();
    this.state = {fixTitle: 'off'};

    this.handleScroll = this.handleScroll.bind(this)
  }

  componentDidMount(){
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount(){
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll(event) {
    var wrapperOffsetTop = this.contentWrapper.current.offsetTop - 70;
    var wrapperOffsetBottom = wrapperOffsetTop + this.contentWrapper.current.clientHeight;
    var titleHeight = this.contentTitle.current.clientHeight + 140; // Account for padding

    if (window.pageYOffset >= wrapperOffsetTop) {
      if ((wrapperOffsetBottom - window.pageYOffset) < titleHeight) {
        if (this.state.fixTitle != 'on2') {
          this.setState({fixTitle: 'on2'});
        }
      } else {
        if (this.state.fixTitle != 'on1') {
          this.setState({fixTitle: 'on1'});
        }
      }
    } else if (this.state.fixTitle != 'off') {
      this.setState({fixTitle: 'off'});
    }
  }

  render() {
    const { slug } = this.props.pageContext
    const postNode = this.props.data.markdownRemark
    const post = postNode.frontmatter
    const fields = postNode.fields

    if (!post.id) {post.id = slug}

    const metaDescription = (postNode.meta ? postNode.meta : postNode.excerpt)

    const htmlAst = postNode.htmlAst
    const renderAst = new rehypeReact({
      createElement: React.createElement,
      components: { "call-to-action": mdCTA },
    }).Compiler

    return (
      <Layout>
        <Helmet>
          <title>{post.title} | {config.siteTitle}</title>
          <meta name="description" content={metaDescription}></meta>
        </Helmet>
        <SEO postPath={slug} postNode={postNode} postSEO />
        <div className={style.page}>
          <div className={style.contentRowBluePurple2}>
            <div className={heroStyle.small} ref={this.heroWrapper}>
              <Link to="/blog"><div className={HeadlineStyle.Headline}>Blog</div></Link>
              <nav className={heroStyle.breadcrumb}>
                <Link to="/blog">Blog</Link>
                <span>{post.title}</span>
              </nav>
            </div>
          </div>

          <div className={style.contentRow}>
            <div className={cn(postStyle.Post, this.state.fixTitle == 'on1' ? postStyle.fixTitle1 : '', this.state.fixTitle == 'on2' ? postStyle.fixTitle2 : '')} ref={this.contentWrapper}>

              <div className={postStyle.postTitle} ref={this.contentTitle}>
                <h5 className={postStyle.postDate}>{post.date}</h5>
                <Headline>{post.title}</Headline>

                {fields && fields.author && <div className={cn(postStyle.author, (fields.author.frontmatter.authorbio ? postStyle.hasBio : ''))}>
                  {!fields.author.frontmatter.portrait && <img src={'/people/'+fields.author.frontmatter.title.toLowerCase().replace(/ /gi,'_') + '.jpg'} />}
                  {fields.author.frontmatter.portrait && <img src={fields.author.frontmatter.portrait} />}
                  <div className={postStyle.info}>
                    <h4>By {fields.author.frontmatter.title} 
                      {fields.author.frontmatter.jobtitle && <span>{fields.author.frontmatter.jobtitle}</span>}
                    </h4>
                    {fields.author.frontmatter.authorbio && <p>{fields.author.frontmatter.authorbio}</p>}
                  </div>
                </div>}

                <div className={postStyle.socialLinks}>
                  <SocialLinks postPath={slug} postNode={postNode} />
                </div>
              </div>
              <div className={postStyle.postContent}>
                <img src={post.cover} />
                {renderAst(htmlAst)}
              </div>
            </div>
          </div>

          <div className={style.contentRowBluePurple}>
            <div className={style.innerWrapper}>
              <Button url="https://info.polecat.com/reputation_management_demo" className={btn.pink}>Book Demo</Button>
            </div>
          </div>
          
          <Footer />
        </div>
      </Layout>
    )
  }
}

/* eslint no-undef: "off"*/
export const pageQuery = graphql`
  query BlogPostBySlug($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      htmlAst
      timeToRead
      frontmatter {
        slug
        meta
        title
        cover
        date(formatString: "MMMM DD, YYYY")
        published
        tags
      }
      fields {
        slug
        author{
          frontmatter{
            title
            jobtitle
            portrait
            authorbio
          }
        }
      }
      excerpt(pruneLength: 155)
    }
  }
`
