/* eslint import/no-unresolved:"off" */
/* eslint import/extensions:"off" */
/* eslint global-require:"off" */
import React from 'react'
import favicon from './favicon.png'

let inlinedStyles = ''
if (process.env.NODE_ENV === 'production') {
  try {
    /* eslint import/no-webpack-loader-syntax: off */
    inlinedStyles = require('!raw-loader!./styles.css')
  } catch (e) {
    /* eslint no-console: "off"*/
    console.log(e)
  }
}

const htmlStyle = {}
const bodyStyle = {}

export default class HTML extends React.Component {
  render() {
    let css
    if (process.env.NODE_ENV === 'production') {
      css = (
        <style
          id="gatsby-inlined-css"
          dangerouslySetInnerHTML={{ __html: inlinedStyles }}
        />
      )
    }
    return (
      <html lang="en" style={htmlStyle}>
        <head>
          <meta charSet="utf-8" />
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0"
          />
          <meta name="google-site-verification" content="T13E6fnkigkbAvvGYkfl4h9hHaY1wHGsXP4pPSDXeH8" />
          <script src="https://script.crazyegg.com/pages/scripts/0051/2753.js"></script>
          <script src="https://cdn.polyfill.io/v2/polyfill.min.js"></script>
          {this.props.headComponents}
          <link rel="shortcut icon" href={favicon} />
          {css}
        </head>
        <body style={bodyStyle}>
          <div
            id="___gatsby"
            dangerouslySetInnerHTML={{ __html: this.props.body }}
          />
          {this.props.postBodyComponents}
        </body>
      </html>
    )
  }
}