import React from 'react'
import cn from 'classnames'

import buttonStyle from '../Button/style.module.css'
import style from './style.module.css'

const SecondaryButton = ({ children, className, url, onClick, target }) => (
  <a href={url ? url : undefined} onClick={onClick ? onClick : undefined} target={target} className={cn(style.SecondaryButton, className, buttonStyle.Button, buttonStyle.pink)}>
    <span>{children}</span>
  </a>
)

SecondaryButton.defaultProps = {
  children: 'Find out more',
  url: '',
  onClick: '',
  target: '_self'
}

export default SecondaryButton
