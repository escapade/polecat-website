import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import Link from 'gatsby-link'

import style from './style.module.css'
import BodyCopy from '../BodyCopy/index';

const Story = ({className, title, children, link, linkText, imageURL}) => (
  <div className={cn(style.className, style.Story)}>
    <div className={style.content}>
      <div>
        <h2>{title}</h2>
        {/* <BodyCopy>{children}</BodyCopy> */}
        <Link url={link}>{linkText}</Link>
      </div>
    </div>
    <div className={style.image} style={{backgroundImage: 'url('+imageURL+')'}}>
      <img src={imageURL} alt={title} />
    </div>
  </div>
)

Story.defaultProps = {
  children: '',
  link: '',
  linkText: '',
  imageURL: ''
};

Story.propTypes ={
  title: PropTypes.node.isRequired,
}

export default Story
