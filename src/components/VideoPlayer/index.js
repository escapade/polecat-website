import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'

import style from './style.module.css'

const VideoPlayer = ({className, children}) => (
  <div className={cn(className, style.VideoPlayer)}>
    {children}
  </div>
)

VideoPlayer.defaultProps = {
  children: '',
  className: ''
};

export default VideoPlayer
