import React from 'react'
import cn from 'classnames'
import Markdown from 'markdown-to-jsx';

import style from './style.module.css'

const Office = ({className, title, children, phone, postcode, country}) => (
  <div className={cn(className, style.Office)}>
    <div className={style.content}>
      <h3>{title}</h3>
      <address>
        {children}
        {postcode && <React.Fragment><br/> {postcode}</React.Fragment>}
        {country && <React.Fragment><br/> {country}</React.Fragment>}
      </address>
      {phone && <a className={style.phone} href={'tel:'+phone}>{phone}</a>}
    </div>
  </div>
)

Office.defaultProps = {
  children: ''
};

export default Office
