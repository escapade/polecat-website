import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'

import style from './style.module.css'
import Headline from '../Headline/index';
import BodyCopy from '../BodyCopy/index';
import SecondaryButton from '../SecondaryButton/index';

const Product = ({className, title, children, link, linkText}) => (
  <div className={cn(className, style.Product)}>
    <div className={style.content}>
      {title && <h3>{title}</h3>}
      {children && <BodyCopy>{children}</BodyCopy>}
      <SecondaryButton url={link}>{linkText}</SecondaryButton>
    </div>
    <div className={style.image}>
      <img src="/assets/repvault.png" />
    </div>
  </div>
)

Headline.defaultProps = {
  children: '',
  link: '',
  linkText: ''
};

Headline.propTypes ={
  title: PropTypes.node.isRequired,
}

export default Product
