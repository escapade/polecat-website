import React from 'react'
import cn from 'classnames'
import Markdown from 'markdown-to-jsx';

import style from './style.module.css'

const Person = ({className, name, position, imageURL, children}) => (
  <div className={cn(className, style.Person)} style={{backgroundImage: 'url('+imageURL+')'}}>
    <div className={style.hitarea} />
    <div className={style.content}>
      <h3>{name}</h3>
      <h4>{position}</h4>
      <Markdown>{children}</Markdown>
    </div>
  </div>
)

Person.defaultProps = {
  name: '',
  position: '',
  imageURL: '',
  children: ''
};

export default Person
