import React from 'react'
import Link from 'gatsby-link'
import cn from 'classnames'
import Headroom from 'react-headroom'

import style from './style.module.css'
import Button from '../Button/index';
import buttonStyle from '../Button/style.module.css';
import LogoLinks from '../LogoLinks/index';

const Header = ({showToggleNav, isActive}) => {
  const toggleNav = ()=>showToggleNav(!isActive);
  const hideNav = ()=>showToggleNav(false);
  return (
    // <Headroom className={cn(style.Headroom, isActive && style.HeadroomOpen)}>
      <header className={style.Header}>
        <div className={style.innerWrapper}>
          <div className={cn(style.sitenav, isActive && style.open, !isActive && style.close)}>
            <div className={style.burger} onClick={toggleNav}>
              Menu
            </div>
            <nav>
              <div>
                <ul>
                  <li><Link to="/" onClick={toggleNav}>Home</Link></li>
                  <li><Link to="/about_us" onClick={toggleNav}>About Us</Link></li>
                  <li><Link to="/product" onClick={toggleNav}>RepVault_</Link></li>
                  <li><Link to="/our_approach" onClick={toggleNav}>Our Approach</Link></li>
                  <li><Link to="/blog" onClick={toggleNav}>Blog</Link></li>
                  <li><Link to="/resources" onClick={toggleNav}>Resources</Link></li>
                  <li><Link to="/careers" onClick={toggleNav}>Careers</Link></li>
                  <li><Link to="/contact" onClick={toggleNav}>Contact Us</Link></li>
                </ul>
                <div className={style.navFooter}>
                  <Button url="https://info.polecat.com/reputation_management_demo" className={cn(style.demo, buttonStyle.pink)}>Book Demo</Button>
                  <LogoLinks />
                </div>
              </div>
            </nav>
            <div className={style.overlay} onClick={toggleNav}/>
          </div>
          <Link to="/" className={style.logo} onClick={hideNav}>
            <img src="/logos/polecat.svg" />
          </Link>
          <nav className={style.loginBtns}>
            <Button url="https://hub.polecat.com" className={cn(style.login, buttonStyle.desktopLink, buttonStyle.pink)}>Log in</Button>
            <Button url="https://info.polecat.com/reputation_management_demo" className={cn(style.demo, buttonStyle.small, buttonStyle.pink)}>Book Demo</Button>
            {/* <button className={style.chat}>Open chat</button> */}
          </nav>
        </div>
      </header>
    // </Headroom>
  )
}

const HeaderEnhanced = class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {active: null}
    this.showToggleNav = (active) => {
      this.setState({active})
    }
  }

  render(){
    return (
      <Header showToggleNav={this.showToggleNav} isActive={this.state.active} />
    )
  }
}
 export default HeaderEnhanced