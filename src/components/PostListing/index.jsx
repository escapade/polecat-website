import React from 'react'
import Link from 'gatsby-link'
import cn from 'classnames'

import style from '../Story/style.module.css'

import BodyCopy from '../BodyCopy/index';

const PostListing = ({ slug, linkText, backgroundImage, title, date, excerpt }) => (
  <Link to={slug} className={cn(style.className, style.Story)}>
    <div className={style.content}>
      <h5 className={style.date}>{date}</h5>
      <h2>{title}</h2>
      {/* <BodyCopy>{excerpt}</BodyCopy> */}
      <span className={style.link}>{linkText}</span>
    </div>
    {backgroundImage && <div className={style.image} style={{backgroundImage: 'url('+backgroundImage+')'}}>
      <img src={backgroundImage} alt={title} />
    </div>}
  </Link>
)

PostListing.defaultProps = {
  linkText: 'Read more'
};

export default PostListing