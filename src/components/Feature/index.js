import React from 'react'
import cn from 'classnames'

import style from './style.module.css'
import BodyCopy from '../BodyCopy/index';

const Feature = ({className, children, iconURL, title}) => (
  <div className={cn(
    (iconURL == 'tick' ? style.small: ''),
    style.Feature)}>

    <div className={style.icon} style={{backgroundImage: 'url(/iconography/'+iconURL+'.svg)'}}>
      <img src={'/iconography/'+iconURL+'.svg'} />
    </div>
    <div className={style.content}>
      <BodyCopy>{title && <h3>{title}</h3>}{children}</BodyCopy>
    </div>
  </div>
)

Feature.defaultProps = {
  title: null,
  children: '',
  iconURL: 'tick'
};

export default Feature
