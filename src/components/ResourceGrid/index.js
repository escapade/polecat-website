import React from 'react'
import Masonry from 'react-masonry-component';
 
import PropTypes from 'prop-types'
import cn from 'classnames'

import style from './style.module.css'

const masonryOptions = {
    transitionDuration: 0
}

const imagesLoadedOptions = {}

const ResourceGrid = ({className, children, isMasonry}) => (
  isMasonry ?
    <Masonry
      className={cn(className, style.ResourceGrid)}
      options={masonryOptions}
      imagesLoadedOptions={imagesLoadedOptions}
      >
      {children}
    </Masonry>
  :
    <div
      className={cn(className, style.ResourceGrid)}
      options={masonryOptions}
      imagesLoadedOptions={imagesLoadedOptions}
      >
      {children}
    </div>
)

ResourceGrid.propTypes ={
  isMasonry: Boolean,
  children: PropTypes.node.isRequired,
}

ResourceGrid.defaultProps = {
  isMasonry: true
}

export default ResourceGrid
