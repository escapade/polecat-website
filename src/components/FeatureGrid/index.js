import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'

import style from './style.module.css'

const FeatureGrid = ({className, children}) => (
  <div className={cn(className, style.FeatureGrid)}>
    {children}
  </div>
)

FeatureGrid.propTypes ={
  children: PropTypes.node.isRequired,
}

export default FeatureGrid
