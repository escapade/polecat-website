import React, { Component } from 'react'
import Link from 'gatsby-link'
import style from './style.module.css'
import LogoLinks from '../LogoLinks/index';
import HotJar from '../Hotjar/index';
import LinkedinTracking from '../LinkedinTracking/index';

class Footer extends Component {
  render() {
    const { config } = this.props
    const url = '/' //config.siteRss
    const copyright = 'Polecat &copy;2018' //config.copyright
    if (!copyright) {
      return null
    }
    return (
      <footer className={style.Footer}>
        <div className={style.gridContainer}>
          <div className={style.logo}>
            <Link to="/">
              <img src="/logos/polecat.svg" />
            </Link>
          </div>
          <div className={style.quickContact}>
            <h4>Quick Contact</h4>
            <nav>
              <a href="tel:+4402070601987">
                +44 (0) 207 060 1987
              </a>
              <a href="mailto:info@polecat.com">
                info@polecat.com
              </a>
            </nav>
          </div>
          <div className={style.quickLinks}>
            <h4>Quick Links</h4>
            <nav>
              <Link to="/about_us">About Us</Link>
              <Link to="/our_approach">Our Approach</Link>
              <Link to="/product">RepVault_</Link>
              <Link to="/blog">Blog</Link>
              <Link to="/careers">Careers</Link>
              <Link to="/contact">Contact Us</Link>
            </nav>
          </div>
          <div className={style.socialLinks}>
            <h4>Follow Us</h4>
            <LogoLinks />
          </div>
        </div>

        <div className={style.subFooter}>
          <div>
            <span>Polecat &copy;2018</span>
            <nav>
              <Link to="/privacy">Privacy Policy</Link>
              <Link to="/terms">Terms &amp; Conditions</Link>
            </nav>
          </div>
        </div>

        <HotJar />
        <LinkedinTracking />
      </footer>
    )
  }
}

export default Footer
