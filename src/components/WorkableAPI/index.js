import React from 'react'
import { StaticQuery, graphql } from "gatsby"

import style from './style.module.css'
import SecondaryButton from '../SecondaryButton/index';

class WorkableAPI extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const jobsdata = this.props.data.jobs.edges;
    // console.log(jobsdata)
    const departmentReducer = (a, {node: {frontmatter: {department, ...job}}}) => ({
      ...a,
      [department]: [...(a[department] ? a[department] : []), job]
    })

    const groupByDepartment = jobs => jobs.reduce(departmentReducer,[])
    
    const jobsdataByDepartment = groupByDepartment(jobsdata)

    return (
      <div className={style.Jobs}>
        {Object.keys(jobsdataByDepartment).map((department, i) => (
          <React.Fragment key={i}>
            <h4 className={style.department}>{department}</h4>
            {jobsdataByDepartment[department].map((edge, i) => (
              <div key={i} className={style.job} target='_blank'>
                <h3>{edge.title}</h3>
                <div className={style.location}>{edge.location.city} - <strong>{edge.location.country_code}</strong></div>
                <div className={style.action}><SecondaryButton url={edge.url} target={'_blank'}>More</SecondaryButton></div>
              </div>
            ))}
          </React.Fragment>
        ))}
      </div>
    )
  }
}

export default props => (
  <StaticQuery
    query={graphql`
      query WorkableQuery {
        jobs: allMarkdownRemark(
          limit: 2000
          sort: { fields: [frontmatter___department], order: ASC }
          filter: {
            fields: { path: { dir: {eq: "workable" }}}
          }
        ) {
          edges {
            node {
              excerpt(pruneLength: 240)
              frontmatter {
                title
                department
                url
                created_at(formatString: "MMMM DD, YYYY")
                location {
                  country
                  country_code
                  city
                }
              }
            }
          }
        }
      }
    `}
    render={data => <WorkableAPI data={data} {...props} />}
  />
)