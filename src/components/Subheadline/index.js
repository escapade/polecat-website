import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import Markdown from 'markdown-to-jsx';

import style from './style.module.css'

const Subheadline = ({children, className}) => (
  <h2 className={cn(className, style.Subheadline)}>
    <Markdown>{children}</Markdown>
  </h2>
)

Subheadline.propTypes ={
  children: PropTypes.node.isRequired
}

export default Subheadline
