import React from 'react'
import scriptLoader from 'react-async-script-loader'

//import style from './style.module.css'
import style from './workable.css'

class Workable extends React.Component {
  constructor(props) {
    super(props);
  }

  componentWillReceiveProps ({ isScriptLoaded, isScriptLoadSucceed }) {
    if (isScriptLoaded && !this.props.isScriptLoaded) { // load finished
      if (isScriptLoadSucceed) {
        this.initWorkable()
      }
      else this.props.onError()
    }
  }

  componentDidMount () {
    const { isScriptLoaded, isScriptLoadSucceed } = this.props
    if (isScriptLoaded && isScriptLoadSucceed) {
      this.initWorkable()
    }
  }

  initWorkable() {
    whr(document).ready(function(){
      console.log('workable')
      whr_embed(80484, {detail: 'titles', base: 'jobs', zoom: 'country', grouping: 'departments'})
    })
  }
  
  render() {
    return (
      <div id="whr_embed_hook"></div>
    )
  }
}


export default scriptLoader(['https://www.workable.com/assets/embed.js'])(Workable)