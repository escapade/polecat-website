import React from 'react'

import style from '../../pages/style.module.css'
import Button from '../Button/index';
import btn from '../Button/style.module.css'

const CTA = ({data}) => (
  <div className={style.contentRowBluePurple}>
    <div className={style.innerWrapper}>
      <Button url={data.url}  className={btn.pink}>{data.calltoaction}</Button>
    </div>
  </div>
)

Button.defaultProps = {
  data: ''
}

export default CTA