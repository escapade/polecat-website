import React from 'react'

import style from './style.module.css'

const LogoLinks = () => (
  <div className={style.LogoLinks}>
    <a href="https://www.facebook.com/Polecat.Intelligence/" target={'_blank'} className={style.fbIcon}>
      Polecat on Facebook
    </a>
    <a href="https://twitter.com/PolecatMM" target={'_blank'} className={style.twitterIcon}>
      Polecat on Twitter
    </a>
    <a href="https://www.linkedin.com/company/polecat/" target={'_blank'} className={style.linkedinIcon}>
      Polecat on LinkedIn
    </a>
    <a href="https://www.instagram.com/polecatincisiveintelligence/" target={'_blank'} className={style.instagramIcon}>
      Polecat on Instagram
    </a>
  </div>
)

export default LogoLinks