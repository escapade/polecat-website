import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'

import style from './style.module.css'

const StoryGrid = ({className, children}) => (
  <div className={cn(className, style.StoryGrid)}>
    {children}
  </div>
)

StoryGrid.propTypes ={
  children: PropTypes.node.isRequired,
}

export default StoryGrid
