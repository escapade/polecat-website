import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'

import style from './style.module.css'
import Headline from '../Headline/index';
import BodyCopy from '../BodyCopy/index';
import Button from '../Button/index';

const Hero = ({className, title, children, link, linkText, imageURL}) => (
  <div className={cn(className, style.Hero)}>
    <div className={style.innerWrapper}>
      <div className={style.copyWrapper}>
        {title && <Headline>{title}</Headline>}
        {children && <BodyCopy>{children}</BodyCopy>}
        {link && <Button url={link}>{linkText}</Button>}
      </div>
      <div className={style.imageWrapper}>
        <img src={imageURL} />
      </div>
    </div>
  </div>
)

Hero.defaultProps = {
  children: '',
  link: '',
  linkText: '',
  className: '',
  imageURL: '/assets/fireflies.png'
};

Hero.propTypes ={
  title: PropTypes.node.isRequired
}

export default Hero
