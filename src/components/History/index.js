import React from 'react'
import Slider from "react-slick";

import Date from '../Date/index';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import s from './style.module.css'
import './scroll.css'

export default class History extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const dates = this.props.dateEdges
    var settings = {
      dots: false,
      infinite: false,
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 4,
      // swipeToSlide: true,
      responsive: [
        {
          breakpoint: 769,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3
          }
        },
        {
          breakpoint: 667,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    }

    return (
      <Slider {...settings} className={s.sliderWrapper}>
        {dates.map((edge, i) => (
          <Date
            key={i}
            date={edge.node.frontmatter.title}
            children={edge.node.html}
          />
        ))}
      </Slider>
    )
  }
}