import React from 'react'
import cn from 'classnames'

import style from './style.module.css'

const KeyPoints = ({className, children, imageURL}) => (
  <div className={cn(className, style.KeyPoints)}>
    <div className={style.innerWrapper}>
      <div className={style.copyWrapper}>
        {children}
      </div>
      <div className={style.imageWrapper}>
        <img src={imageURL} />
      </div>
    </div>
  </div>
)

KeyPoints.defaultProps = {
  children: '',
  className: '',
  imageURL: '/assets/fireflies.svg'
};

export default KeyPoints
