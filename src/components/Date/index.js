import React from 'react'
import cn from 'classnames'

import style from './style.module.css'
import BodyCopyStyle from '../BodyCopy/style.module.css';

const Date = ({date, children}) => (
  <div className={cn(style.className, style.Date)}>
    <h4>{date}</h4>
    <div className={BodyCopyStyle.BodyCopy} dangerouslySetInnerHTML={{ __html: children }}/>
  </div>
)

Date.defaultProps = {
  date: '',
  children: ''  
};

export default Date
