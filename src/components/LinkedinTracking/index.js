import React from 'react'
import scriptLoader from 'react-async-script-loader'

import config from '../../../data/SiteConfig'

class LinkedinTracking extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount () {
    const { isScriptLoaded, isScriptLoadSucceed } = this.props
    if (isScriptLoaded && isScriptLoadSucceed) {
      this.init()
    }
  }

  init() {
    console.log('Linkedin loaded')
    var _linkedin_partner_id = config.linkedinPartnerID;
    window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
    window._linkedin_data_partner_ids.push(_linkedin_partner_id);
  }
  
  render() {
    return null
  }
}

export default scriptLoader(['https://snap.licdn.com/li.lms-analytics/insight.min.js'])(LinkedinTracking)

