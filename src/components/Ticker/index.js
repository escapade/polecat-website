import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'

import style from './style.module.css'

const Ticker = ({children, className}) => (
  <div className={cn(className, style.Ticker)}>
    <div className={style.innerWrapper}>
      <h4>Latest News</h4>
      <div className={style.messages}>
        <ul>
          <li>{children}</li>
          <li>{children}</li>
        </ul>
      </div>
    </div>
  </div>
)

export default Ticker
