import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import Dotdotdot from 'react-dotdotdot'


import style from './style.module.css'

const BodyCopy = ({className, children, dots}) => (
  <div className={cn(className, style.BodyCopy)}>
    {dots && <Dotdotdot clamp={'auto'}>{children}</Dotdotdot>}
    {!dots && children}
  </div>
)

BodyCopy.propTypes ={
  children: PropTypes.node.isRequired
}

BodyCopy.defaultProps = {
  dots: false
}

export default BodyCopy
