import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import Markdown from 'markdown-to-jsx';

import style from './style.module.css'

const Headline = ({children, className }) => (
  <h1 className={cn(className, style.Headline)}>
    <Markdown>{children}</Markdown>
  </h1>
)

Headline.propTypes = {
  children: PropTypes.node.isRequired
}

export default Headline

