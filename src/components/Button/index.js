import React from 'react'
import cn from 'classnames'

import style from './style.module.css'

const Button = ({ children, onClick, className , url, target}) => (
  <a href={url ? url : undefined} onClick={onClick ? onClick : undefined} target={target} className={cn(className, style.Button)}>
    <span>{children.toLowerCase()}</span>
  </a>
)

Button.defaultProps = {
  children: 'Find out more',
  url: '',
  onClick: '',
  target: '_self'
}

export default Button
