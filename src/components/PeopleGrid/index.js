import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'

import style from './style.module.css'

const PeopleGrid = ({className, children}) => (
  <div className={cn(className, style.PeopleGrid)}>
    {children}
  </div>
)

PeopleGrid.propTypes ={
  children: PropTypes.node.isRequired,
}

export default PeopleGrid
