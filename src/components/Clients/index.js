import React from 'react'
import cn from 'classnames'

import style from './style.module.css'
import Subheadline from '../Subheadline/index';

const Clients = ({className, title, clients}) => (
  <div className={cn(className, style.Clients)}>
    <Subheadline>{title}</Subheadline>
    <div className={style.grid}>
      {clients.map((edge, i) => (
        <div key={i} className={style.gridItem} style={{backgroundImage: 'url('+edge.logo+')'}} title={edge.name} >
          {edge.name}
        </div>
      ))}
    </div>
  </div>
)

export default Clients
