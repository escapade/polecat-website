import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'

import style from '../RegisterForm/style.module.css'
import Subheadline from '../Subheadline/index';
import BodyCopy from '../BodyCopy/index';
import buttonStyle from '../Button/style.module.css';
import secondButtonStyle from '../SecondaryButton/style.module.css';


const ContactForm = ({className, title, submitURL, buttonText, children}) => (
  <div className={cn(className, style.Form)}>
    <Subheadline>{title}</Subheadline>
    <BodyCopy>{children}</BodyCopy>

    <form action={submitURL} method="POST">
      <div>
        <p>* All fields are required</p>
        <input name="first_name" type="text" placeholder="First Name *" className={style.halfWidth} required/>
        <input name="last_name" type="text" placeholder="Last Name *" className={style.halfWidth} required/>
        <input name="job_title" type="text" placeholder="Job Title *" className={style.halfWidth} required/>
        <input name="company" type="text" placeholder="Company Name *" className={style.halfWidth} required/>
        <input name="phone" type="text" placeholder="Phone *" className={style.halfWidth} required/>
        <input name="email" type="email" placeholder="Email Address *" className={style.halfWidth} required/>
        <input name="message" type="text" placeholder="Interested in&hellip; *" required/>
        <input name="tandcs" type="hidden" value="1"/>
        <input name="opt_in" type="hidden" value="1"/>
        <input name="success" type="hidden" value="http://polecat.com" />
      </div>
      <button type="submit" className={cn(secondButtonStyle.Button, buttonStyle.Button, buttonStyle.pink)}><span>{buttonText}</span></button>
      <p className="styles.tandcs">By signing up I agree to the terms of service and privacy policy</p>
    </form>
  </div>
)

ContactForm.defaultProps = {
  children: ''
};

ContactForm.propTypes ={
  title: PropTypes.node.isRequired,
  submitURL: PropTypes.node.isRequired,
  buttonText: PropTypes.node.isRequired
}

export default ContactForm
