import React from 'react'
import cn from 'classnames'

import style from './style.module.css'
import BodyCopy from '../BodyCopy/index';

import btn from '../../components/Button/style.module.css'
import SecondaryButton from '../SecondaryButton/index';

const Resource = ({resourceType, title, subtitle, children, linkURL, linkText, imageURL, companyName, companyLogo, download, target}) => (
  <div className={cn(
    (resourceType == 'case_study' ? style.case_study: ''), 
    (resourceType == 'whitepaper' ? style.whitepaper: ''), 
    (resourceType == 'quotation' ? style.quotation: ''), 
    (resourceType == 'report' ? style.report: ''),
    (resourceType == 'micro' ? style.micro: ''),
    (resourceType == 'insight' ? style.insight: ''), 
    (resourceType == 'featured_post' ? style.featured_post: ''),
    (resourceType == 'latest_post' ? style.latest_post: ''),
    (resourceType == 'featured_resource' ? style.featured_resource: ''),
    (resourceType == 'latest_resource' ? style.latest_resource: ''),
    style.Resource)}>
    {/* {imageURL && 
      <div className={style.image} style={{backgroundImage: 'url('+imageURL+')'}}>
        <img src={imageURL} alt={title} />
      </div>
    } */}
    <div className={style.content}>
      {resourceType == 'quotation' ? '' : <div className={style.type} />}
      {imageURL && 
        <div className={style.image} style={{backgroundImage: 'url('+imageURL+')'}}>
          <img src={imageURL} alt={title} />
        </div>
      }
      {title && 
        resourceType == 'quotation' ? '' :
        ( linkURL || download ? (
          <React.Fragment>
            {linkURL && <h2><a href={linkURL} target={target}>{title}</a></h2>}
            {download && <h2><a href={download} target={target}>{title}</a></h2>}
          </React.Fragment>
        ) : (
          <h2>{title}</h2>
        )
      )}
      {subtitle && <h3>{subtitle}</h3>}
      {children && resourceType == 'quotation' ? <blockquote>{children}</blockquote> : <BodyCopy>{children}</BodyCopy>}
      {linkURL && <SecondaryButton url={linkURL} target={target} className={cn(style.cta, btn.pink)}>{linkText}</SecondaryButton>}
      {download && <SecondaryButton url={download} target={target} className={cn(style.cta, btn.pink, btn.download)}>Download</SecondaryButton>}
      {companyLogo && <img className={style.logoIMG} src={companyLogo} alt={companyName} />}
    </div>
  </div>
)

Resource.defaultProps = {
  className: '',
  target: "_blank",
  linkText: 'Find out more'
};

export default Resource
