import React from 'react'
import Helmet from 'react-helmet'

import Header from '../Header'
import config from '../../../data/SiteConfig'
import '../../styles.css'

export default class Layout extends React.Component {
  render() {
    const { children } = this.props
    return (
      <React.Fragment>
        <Helmet>
          <title>{`${config.siteTitle}`}</title>
        </Helmet>
        <Header />
        <main>{children}</main>
      </React.Fragment>
    )
  }
}
