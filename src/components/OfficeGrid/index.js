import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'

import style from './style.module.css'

const OfficeGrid = ({className, children}) => (
  <div className={cn(className, style.OfficeGrid)}>
    {children}
  </div>
)

OfficeGrid.propTypes ={
  children: PropTypes.node.isRequired,
}

export default OfficeGrid
