import React from 'react'
import ReactModal from 'react-modal'
import cn from 'classnames'

import style from './style.module.css'

ReactModal.setAppElement('#___gatsby')

const Modal = ({ className, overlayClassName, ...props }) => (
  <ReactModal
    {...props}
    className={cn(style.Modal, className)}
    overlayClassName={cn(style.ModalOverlay, overlayClassName)}
  />
)

export default Modal
