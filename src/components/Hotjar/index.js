import React from 'react'

class HotJar extends React.Component {
  constructor(props) {
    super(props);
  }

  componentWillReceiveProps () {
    this.initHotjar()
  }

  componentDidMount () {
    this.initHotjar()
  }

  initHotjar() {
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:968321,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
  }
  
  render() {
    return null
  }
}


export default HotJar