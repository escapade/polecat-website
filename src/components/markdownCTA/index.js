import React from 'react'
import { StaticQuery, graphql } from "gatsby"

import Button from '../Button/index';
import btn from '../Button/style.module.css'
import style from './style.module.css'

const Mdcta = ({ data, cta }) => (
  data.allMarkdownRemark.edges.map((edge, i) => (
    edge.node.frontmatter.title == cta ?
      edge.node.frontmatter.image ?
      <a key={i} href={edge.node.frontmatter.url} className={style.mdCTA} target='_blank'>
        <img src={edge.node.frontmatter.image} alt={edge.node.frontmatter.calltoaction} title={edge.node.frontmatter.calltoaction}/>
      </a>
      : <Button key={i} url={edge.node.frontmatter.url} className={btn.pink} target={'_blank'}>{edge.node.frontmatter.calltoaction}</Button>
    : ''
  ))
)

export default props => (
  <StaticQuery
    query={graphql`
      query CTAQuery {    
        allMarkdownRemark(
          filter: {
            frontmatter: {
              ctapost: { eq: true }
            }
          }
        ) {
          edges {
            node {
              frontmatter {
                title
                calltoaction
                image
                url
              }
            }
          }
        }
      }
    `}
    render={data => <Mdcta data={data} {...props} />}
  />
)