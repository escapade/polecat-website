import ApercoBlackTTF from "/fonts/aperco_black/aperco-black.ttf";
import ApercoBlackWOFF from "/fonts/aperco_black/aperco-black.woff";
import ApercoBlackWOFF2 from "/fonts/aperco_black/aperco-black.woff2";
import ApercoBlackEOT from "/fonts/aperco_black/aperco-black.eot";

import ApercoBoldTTF from "/fonts/aperco_bold/aperco-bold.ttf";
import ApercoBoldWOFF from "/fonts/aperco_bold/aperco-bold.woff";
import ApercoBoldWOFF2 from "/fonts/aperco_bold/aperco-bold.woff2";
import ApercoBoldEOT from "/fonts/aperco_bold/aperco-bold.eot";

import ApercoLightTTF from "/fonts/aperco_light/aperco-light.ttf";
import ApercoLightWOFF from "/fonts/aperco_light/aperco-light.woff";
import ApercoLightWOFF2 from "/fonts/aperco_light/aperco-light.woff2";
import ApercoLightEOT from "/fonts/aperco_light/aperco-light.eot";

import ApercoMediumTTF from "/fonts/aperco_medium/aperco-medium.ttf";
import ApercoMediumWOFF from "/fonts/aperco_medium/aperco-medium.woff";
import ApercoMediumWOFF2 from "/fonts/aperco_medium/aperco-medium.woff2";
import ApercoMediumEOT from "/fonts/aperco_medium/aperco-medium.eot";

import ApercoMonoTTF from "/fonts/aperco_mono/aperco-mono.ttf";
import ApercoMonoWOFF from "/fonts/aperco_mono/aperco-mono.woff";
import ApercoMonoWOFF2 from "/fonts/aperco_mono/aperco-mono.woff2";
import ApercoMonoEOT from "/fonts/aperco_mono/aperco-mono.eot";

import ApercoRegularTTF from "/fonts/aperco_regular/aperco-regular.ttf";
import ApercoRegularWOFF from "/fonts/aperco_regular/aperco-regular.woff";
import ApercoRegularWOFF2 from "/fonts/aperco_regular/aperco-regular.woff2";
import ApercoRegularEOT from "/fonts/aperco_regular/aperco-regular.eot";

export {
  ApercoBlackTTF,
  ApercoBlackWOFF,
  ApercoBlackWOFF2,
  ApercoBlackEOT,
  ApercoBoldTTF,
  ApercoBoldWOFF,
  ApercoBoldWOFF2,
  ApercoBoldEOT,
  ApercoLightTTF,
  ApercoLightWOFF,
  ApercoLightWOFF2,
  ApercoLightEOT,
  ApercoMediumTTF,
  ApercoMediumWOFF,
  ApercoMediumWOFF2,
  ApercoMediumEOT,
  ApercoMonoTTF,
  ApercoMonoWOFF,
  ApercoMonoWOFF2,
  ApercoMonoEOT,
  ApercoRegularTTF,
  ApercoRegularWOFF,
  ApercoRegularWOFF2,
  ApercoRegularEOT
}
