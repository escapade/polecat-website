const path = require('path')
const _ = require('lodash')

const config = require('./data/SiteConfig')

const isBlogPost = node =>  node && node.frontmatter && node.frontmatter.blogpost === true

const isResource = node =>  node && node.frontmatter && node.frontmatter.resourcepost === true

const isMarkdown = node => node && node.internal.type === 'MarkdownRemark'

const imageExtensions = ['jpeg', 'jpg', 'png', 'webp', 'tif', 'tiff']
const isImagePath = imagePath =>
  imageExtensions.some(ext => imagePath.endsWith(ext))

exports.onCreateNode = ({ node, actions, getNode, getNodes }) => {
  const { createNodeField, createParentChildLink } = actions

  if (isMarkdown(node) && (isBlogPost(node) || isResource(node))) {
    let slug
    let slugSan
    const fileNode = getNode(node.parent)
    const filePath = (isBlogPost(node) ? '/blog/' : '/resource/')
    const parsedFilePath = path.parse(fileNode.relativePath)
    if (node && node.frontmatter && node.frontmatter.slug) {
      slugSan = node.frontmatter.slug.replace(/[^A-Za-z0-9-]/g, "")
      slug = `${filePath}${_.kebabCase(slugSan)}`
    } else {
      slug = `${filePath}${parsedFilePath.name}/`
    }
    createNodeField({ node, name: 'slug', value: slug })
  }

  // Create paths fields
  if (isMarkdown(node)) {
    const fileNode = getNode(node.parent)
    const parsedFilePath = path.parse(fileNode.relativePath)
    createNodeField({ node, name: 'path', value: parsedFilePath })
  }

  if (isMarkdown(node) && isBlogPost(node)) {
    const value = node.frontmatter.cover
    if (typeof value === 'string' && isImagePath(value)) {
      const pathToFile = path.join(__dirname, 'static', value)
      const fileNode = getNodes().find(n => n.absolutePath === pathToFile)
      if (fileNode != null) {
        const imageSharpNodeId = fileNode.children.find(n =>
          n.endsWith('>> ImageSharp')
        )
        const imageSharpNode = getNodes().find(n => n.id === imageSharpNodeId)
        if (imageSharpNode) {
          createParentChildLink({ parent: node, child: imageSharpNode })
        }
      }
    }
  }
}

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions

  return new Promise((resolve, reject) => {
    const postPage = path.resolve('src/templates/post.jsx')
    const tagPage = path.resolve('src/templates/tag.jsx')
    resolve(
      graphql(`
        {
          allMarkdownRemark(
            filter: { 
              frontmatter: { 
                blogpost: { eq: true }
                ${config.siteUrl != config.stagingUrl ? 'published: { ne: false }' : ''}
              } 
            }
          ) {
            edges {
              node {
                frontmatter {
                  tags
                }
                fields {
                  slug
                }
              }
            }
          }
        }
      `)
      .then(result => {
        if (result.errors) {
          /* eslint no-console: "off" */
          console.log(result.errors)
          reject(result.errors)
        }

        const tagSet = new Set()
        result.data.allMarkdownRemark.edges.forEach(edge => {
          if (edge.node.frontmatter.tags) {
            edge.node.frontmatter.tags.forEach(tag => {
              tagSet.add(tag)
            })
          }

          createPage({
            path: edge.node.fields.slug,
            component: postPage,
            context: {
              slug: edge.node.fields.slug,
            },
          })
        })

        const tagList = Array.from(tagSet)
        tagList.forEach(tag => {
          createPage({
            path: `/tags/${_.kebabCase(tag)}/`,
            component: tagPage,
            context: {
              tag,
            },
          })
        })
      })
    )
  })
}

exports.onCreateBabelConfig = ({ actions }) => {
  actions.setBabelPlugin({
    name: `babel-plugin-transform-regenerator`,
  })
}

exports.sourceNodes = ({ actions, getNodes, getNode }) => {
  const { createNodeField } = actions;

  const featuredPosts = {};
  const featuredResources = {};
  const authors = {};
  const ctas = {};

  const markdownNodes = getNodes()
    .filter(node => node.internal.type === "MarkdownRemark")
    .forEach(node => {
      if (node.frontmatter.author) {
        const postNode = getNodes().find(
          node2 => node2.internal.type === "MarkdownRemark" && node2.frontmatter.title === node.frontmatter.author
        );

        if (postNode) {
          createNodeField({
              node,
              name: "author",
              value: postNode.id,
          });

          // if it's first time for this author init empty array for his posts
          if (!(postNode.id in authors)) {
            authors[postNode.id] = [];
          }
          // add book to this author
          authors[postNode.id].push(node.id);
        }
      }
      if (node.frontmatter.cta) {
        const postNode = getNodes().find(
          node2 => node2.internal.type === "MarkdownRemark" && node2.frontmatter.title === node.frontmatter.cta
        );

        if (postNode) {
          createNodeField({
              node,
              name: "cta",
              value: postNode.id,
          });

          // if it's first time for this author init empty array for his posts
          if (!(postNode.id in ctas)) {
            ctas[postNode.id] = [];
          }
          // add book to this author
          ctas[postNode.id].push(node.id);
        }
      }
    });

  Object.entries(featuredPosts).forEach(([postNodeId, postIds]) => {
    createNodeField({
      node: getNode(postNodeId),
      name: "posts",
      value: postIds,
    });
  });
};
